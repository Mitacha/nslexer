module.exports = {
    context: __dirname,
    entry: "./src/lexer.js",
    output: {
        filename: "lexer.js",
        path: __dirname + "/dist",
    },
    resolve: {
        extensions: ['', '.js']
    },
    module: {
        loaders: [
            {
                test: /\.js$/,
                exclude: /node_modules/,
                loaders: ["babel-loader"]
            }
        ]
    }
};