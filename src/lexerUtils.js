/**
 * @callback UnaryPredicate
 * @param a
 * @returns {Boolean}
 */

/**
 * Checks if 'char' is a whitespace
 * @param {string} char
 * @returns Boolean
 */
function isWhitespace(char) {
    return char == ' ' || char == '\n' || char == '\t';
}

/**
 * Checks if 'char' is a beginning of number
 * - or digit
 * @param {string} char
 * @returns Boolean
 */
function isNumberStart(char) {
    return /[\-\d]/g.test(char);
}

/**
 * Checks if 'char' is a digit
 * @param {string} char
 * @returns Boolean
 */
function isDigit(char) {
    return /\d/g.test(char);
}

/**
 * Checks if 'char' is beginning of identifier
 * @param {string} char
 * @returns Boolean
 */
function isIdentifierStart(char) {
    return /[A-Za-z]/g.test(char);
}

/**
 * Checks if 'char' is allowed in identifier
 * @param {string} char
 * @returns Boolean
 */
function isIdentifier(char) {
    return /[\w\d!@#?]/g.test(char);
}

/**
 * Checks if 'char' is a punctuation
 * @param {string} char
 * @returns Boolean
 */
function isPunctuation(char) {
    return /[().,;\[\]{}:=<>/]/g.test(char);
}

/**
 * As long as predicate returns true, skips range's front
 * @param {Range} range
 * @param {UnaryPredicate}predicate
 */
function skipUntil(range, predicate) {
    while (!range.empty() && predicate(range.front())) {
        range.popFront();
    }
}

/**
 * Invokes popFront n times on target range
 * If range provides its own implementation of popFrontN,
 * its called.
 * @param {Range} range
 * @param {Number} n
 */
function popFrontN(range, n) {
    if (range.hasOwnProperty('popFrontN')) {
        range.popFrontN(n);
    } else {
        while (n > 0) {
            range.popFront();
            --n;
        }
    }
}
/**
 * Takes elements from current position to delimiter
 * If delimiter is prepend with \(backslash) than delimiter is ignored
 * @param {InputRange} inputRange
 * @param {string} delimiter - length == 1
 * @returns {string}
 */
function takeEscaped(inputRange, delimiter) {
    let input = inputRange.input.slice(inputRange.position, -1) + delimiter; // put sentinel
    let inputLength = inputRange.input.length;
    let i = inputRange.position - 1;
    do {
        i++;
        while (input[i] != delimiter) {
            i++;
        }
    } while (input[i] == delimiter && input[i-1] == '\\');
    input = inputRange.input;
    let found = (i < inputLength-1) || ((input[inputLength - 1] == delimiter));
    let start = inputRange.position;
    let end = found ? start + i : inputLength;
    let offset = end - start;
    popFrontN(inputRange, offset > inputLength ? inputLength : offset + 1);
    return input.slice(start, end);
}

/**
 * Takes elements from range, while predicate is true
 * @param {Range} inputRange
 * @param {UnaryPredicate} predicate
 * @returns {*}
 */
function takeWhile(inputRange, predicate) {
    let value = '';
    for (;!inputRange.empty() && predicate(inputRange.front()); inputRange.popFront()) {
        value += inputRange.front();
    }
    return value;
}
/**
* @callback ForEachCallback
* @param element - range element
* @param index - 0-based index
*/
/**
 * Foreach implementation for ranges
 * @param {Range} range
 * @param {ForEachCallback} fn
 */
function rangeForEach(range, fn) {
    for (let i = 0;!range.empty();range.popFront()) {
        fn(range.front(), i++);
    }
}

module.exports = {
    isWhitespace: isWhitespace,
    isDigit: isDigit,
    isNumberStart: isNumberStart,
    isIdentifier: isIdentifier,
    isIdentifierStart: isIdentifierStart,
    isPunctuation: isPunctuation,
    skipUntil: skipUntil,
    takeEscaped: takeEscaped,
    takeWhile: takeWhile,
    popFrontN: popFrontN,
    rangeForEach: rangeForEach
};
