const lexerUtils = require('./lexerUtils');

class InputRange {
    constructor(input, startPosition = 0, column = 1, line = 1) {
        this.input = input;
        this.position = startPosition;
        this.column = column;
        this.line = line;
    }
    front() {
        return this.input.charAt(this.position);
    }
    popFront() {
        this.position++;
        this.column++;
        if (!this.empty() && this.input.charAt(this.position) == '\n') {
            this.position++;
            this.line++;
            this.column = 1;
        }
    }
    empty() {
        return this.front() == '';
    }
}

class Lexer {
    constructor(inputRange) {
        this.inputRange = inputRange;
        this.current = null;
    }
    front() {
        if (this.current == null) {
            this.popFront();
        }
        return this.current;
    }
    popFront() {
        lexerUtils.skipUntil(this.inputRange, lexerUtils.isWhitespace);
        if (this.inputRange.empty()) {
            this.current = {type: 'EMPTY', column: this.inputRange.column, line: this.inputRange.line};
            return this.current;
        }
        let char = this.inputRange.front();
        if (char == '"' || char == "'") {
            this.current = readString(this.inputRange, char);
            return this.current;
        }
        if (lexerUtils.isNumberStart(char)) {
            this.current = readNumber(this.inputRange);
            return this.current;
        }
        if (lexerUtils.isIdentifierStart(char)) {
            this.current = readIdentifier(this.inputRange);
            return this.current;
        }
        if (lexerUtils.isPunctuation(char)) {
            this.current = readPunctuation(this.inputRange);
            return this.current;
        }

        throw new LexerError("Cannot handle character: " + char, this.column, this.line);
    }
    empty() {
        return this.front().type == 'EMPTY';
    }
    get line() {
        return this.inputRange.line;
    }
    get column() {
        return this.inputRange.column;
    }
}

function getPosition(inputRange) {
    return {
        column: inputRange.column,
        line: inputRange.line
    };
}

function readString(inputRange, apostrophe) {
    let startPos = getPosition(inputRange);
    inputRange.popFront();
    let strValue = lexerUtils.takeEscaped(inputRange, apostrophe);
    return {
        type: "str",
        value: strValue,
        column: startPos.column,
        line: startPos.line
    }
}

function readNumber(inputRange) {
    let startPos = getPosition(inputRange);

    let value = inputRange.front();
    inputRange.popFront();

    let hasDot = value == '.';
    while (!inputRange.empty()) {
        let char = inputRange.front();
        if (char == '.') {
            if (hasDot) {
                break;
            }
            hasDot = true;
            value += char;
            inputRange.popFront();
            continue;
        }

        if (lexerUtils.isDigit(char)) {
            value += char;
            inputRange.popFront();
        } else {
            break;
        }
    }
    return {
        type: 'num',
        value: Number.parseFloat(value),
        column: startPos.column,
        line: startPos.line
    }
}

function readIdentifier(inputRange) {
    let startPos = getPosition(inputRange);
    let value = inputRange.front();
    inputRange.popFront();
    value += lexerUtils.takeWhile(inputRange, lexerUtils.isIdentifier);
    return {
        type: 'sym',
        value: value,
        column: startPos.column,
        line: startPos.line
    }
}

function readPunctuation(inputRange) {
    let startPos = getPosition(inputRange);
    let value = inputRange.front();
    inputRange.popFront();

    value += lexerUtils.takeWhile(inputRange, lexerUtils.isPunctuation);

    return {
        type: 'punc',
        value: value,
        column: startPos.column,
        line: startPos.line
    }
}

function LexerError(message, column, line) {
    this.name = 'LexerError';
    this.message = message;
    this.column = column;
    this.line = line;
}

module.exports = {
    InputRange: InputRange,
    Lexer: Lexer
};