const InputRange = require('../src/lexer').InputRange;
const Lexer = require('../src/lexer').Lexer;
const lexerUtils = require('../src/lexerUtils');

const chai = require('chai');

var assert = chai.assert;

describe('LexerUtils', function () {
    it('should skip whitespaces', function () {
        let range = new InputRange('     \t\t\n\ttest  test2');
        lexerUtils.skipUntil(range, lexerUtils.isWhitespace);
        assert.equal(range.input.slice(range.position), 'test  test2');
    });
    it('should skip any predicate', function () {
        let range = new InputRange('test  test2');
        lexerUtils.skipUntil(range, ch => 'test'.includes(ch));
        lexerUtils.skipUntil(range, lexerUtils.isWhitespace);
        assert.equal(range.input.slice(range.position), 'test2');
    });
    it('should return escaped value', function () {
        let range = new InputRange('string"');
        assert.equal(lexerUtils.takeEscaped(range, `"`), 'string');
        assert.equal(range.position, 7);

        let range2 = new InputRange('str\\"ing"');
        assert.equal(lexerUtils.takeEscaped(range2, `"`), 'str\\"ing');
        assert.equal(range2.position, 9);

        let range3 = new InputRange(`nope`);
        assert.equal(lexerUtils.takeEscaped(range3, `"`), `nope`);
        assert.equal(range3.position, 5);
    });
    it('should popFront 5 times', function () {
        let range = new InputRange('1234542');
        lexerUtils.popFrontN(range, 5);
        assert.equal(range.position, 5);
        assert.equal(range.input.slice(range.position), '42');
    });
    it('should popFront, using custom popFrontN', function () {
        let range = {
            popFrontN: function () {
                this.done = true
            },
            done: false
        };
        lexerUtils.popFrontN(range, 5);
        assert.isTrue(range.done);
    });
});
describe('Lexer', function () {
    it('should lex positive numbers', function () {
        let inputRange = new InputRange(`1 23 45 678`);
        let lexer = new Lexer(inputRange);
        let expected = [1,23,45,678];
        lexerUtils.rangeForEach(lexer, (token, i) => {
            assert.equal(token.type, "num");
            assert.equal(token.value, expected[i]);
        });
    });
    it('should lex negative numbers', function () {
        let inputRange = new InputRange(`-1 -23 -45  -678`);
        let lexer = new Lexer(inputRange);
        let expected = [-1,-23,-45,-678];
        lexerUtils.rangeForEach(lexer, (token, i) => {
            assert.equal(token.type, "num");
            assert.equal(token.value, expected[i]);
        });
    });
    it('should lex real numbers', function () {
        let inputRange = new InputRange(`1 2.3  4.5777  -63.0111`);
        let lexer = new Lexer(inputRange);
        let expected = [1,2.3,4.5777,- 63.0111];
        lexerUtils.rangeForEach(lexer, (token, i) => {
            assert.equal(token.type, "num");
            assert.equal(token.value, expected[i]);
        });
    });
    it('should return string number and dot', function () {
        let inputRange = new InputRange(`"aa" 123    .`);
        let lexer = new Lexer(inputRange);

        let strToken = lexer.front();
        assert.equal(strToken.type, "str");
        assert.equal(strToken.value, "aa");
        assert.equal(strToken.line, 1);
        assert.equal(strToken.column, 1);
        lexer.popFront();

        let numToken = lexer.front();
        assert.equal(numToken.type, "num");
        assert.equal(numToken.value, "123");
        assert.equal(numToken.line, 1);
        assert.equal(numToken.column, 6);
        lexer.popFront();

        let dotToken = lexer.front();
        assert.equal(dotToken.type, "punc");
        assert.equal(dotToken.value, ".");
        assert.equal(dotToken.line, 1);
        assert.equal(dotToken.column, 13);
        lexer.popFront();

        assert.isTrue(lexer.empty());
    });
    it('should increase line after new line', function () {
        let inputRange = new InputRange('let num = 123\nnum = 321');
        let lexer = new Lexer(inputRange);
        lexerUtils.popFrontN(lexer, 6);

        assert.equal(lexer.line, 2);
    });
    it('should lex simple function call', function () {
        let inputRange = new InputRange("writeln('Hello, world')");
        let lexer = new Lexer(inputRange);

        let funToken = lexer.front();
        assert.equal(funToken.type, "sym");
        assert.equal(funToken.value, "writeln");
        assert.equal(funToken.line, 1);
        assert.equal(funToken.column, 1);
        lexer.popFront();

        let lbracketToken = lexer.front();
        assert.equal(lbracketToken.type, "punc");
        assert.equal(lbracketToken.value, "(");
        assert.equal(lbracketToken.line, 1);
        assert.equal(lbracketToken.column, 8);
        lexer.popFront();

        let strToken = lexer.front();
        assert.equal(strToken.type, "str");
        assert.equal(strToken.value, "Hello, world");
        assert.equal(strToken.line, 1);
        assert.equal(strToken.column, 9);
        lexer.popFront();

        let rbracketToken = lexer.front();
        assert.equal(rbracketToken.type, "punc");
        assert.equal(rbracketToken.value, ")");
        assert.equal(rbracketToken.line, 1);
        assert.equal(rbracketToken.column, 23);
        lexer.popFront();
    });
    it('should lex simple assignments call', function () {
        let inputRange = new InputRange("let num = 123\nnum = 321");
        let lexer = new Lexer(inputRange);

        let expectedTypes = ['sym', 'sym', 'punc', 'num', 'sym', 'punc', 'num'];
        let expectedValues = ['let', 'num', '=', '123', 'num', '=', '321'];
        let expectedColumns = [1,5,9,11,1,5,7];
        let expectedLines = [1,1,1,1,2,2,2];

        lexerUtils.rangeForEach(lexer, (token, i) => {
            assert.equal(token.type, expectedTypes[i]);
            assert.equal(token.value, expectedValues[i]);
            assert.equal(token.column, expectedColumns[i]);
            assert.equal(token.line, expectedLines[i]);
        });

    });
});